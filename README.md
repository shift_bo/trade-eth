# ETH量化交易

    └─strategy 策略

## 相关文档地址

    [币安API]https://binance-docs.github.io/apidocs/futures/cn/#sdk
    

## 项目功能  
    预计实现：
        1. 实现行情监控
        2. 根据策略进行提示
        3. 实现根据策略进行交易
## 依赖安装  
    - 

## 项目目录

## 策略  

    1. 回撤资金  
        每当盈利之后锁定一部分资金
    2. 目标1 
        买卖条件 (1)middle翻转 买入 （2）top 或 bottom 卖出
        全仓 100u*100
        止损 100u兜底
        统计胜率