from binance.websocket.futures.websocket_client import FuturesWebsocketClient as WebsocketClient
ws_client = WebsocketClient()
ws_client.start()
ws_client.mini_ticker(
    symbol='bnbusdt',
    id=1,
    callback=message_handler,
)